package com.capgemini.bowling.calculator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class BowlingGameResultCalculatorInitTest {

	BowlingGameResultCalculatorInit calculator;
	
	@Before
	public void setUp() throws Exception {
		calculator = new BowlingGameResultCalculatorInit();
	}

	@Test
	public void shouldNotFinishGameWhenFinalRoundIsNotFinished() throws Exception {
		calculator.roll(10);
		calculator.roll(2);
		calculator.roll(6);
		assertFalse(calculator.isFinished());
	}
	
	@Test
	public void shouldFinishGameWhenFinalRoundFinished() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		assertTrue(calculator.isFinished());
	}
	
	@Test
	public void shouldReturnMaxScore() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		assertEquals(300, calculator.score());
	}
	
	@Test
	public void shouldAdd2BonusScoreToRoundWhenIsOneRollStrike() throws Exception {
		calculator.roll(10);
		calculator.roll(2);
		calculator.roll(2);
		assertEquals(18, calculator.score());
	}

	@Test
	public void shouldAdd1BonusScoreToRoundWhenIsTwoRollStrike() throws Exception {
		calculator.roll(8);
		calculator.roll(2);
		calculator.roll(2);
		assertEquals(14, calculator.score());
	}
	
	@Test
	public void shouldNotAddBonusScoreToRoundWhenRoundScoreLess10() throws Exception {
		calculator.roll(8);
		calculator.roll(1);
		calculator.roll(2);
		assertEquals(11, calculator.score());
	}
	
	@Test
	public void shouldAddBonusRollInFinalRoundWhenIsTwoRollStrike() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(8);
		calculator.roll(2);
		calculator.roll(1);
		assertEquals(269, calculator.score());
	}
	
	@Test
	public void shouldAddBonusRollInFinalRoundWhenIsOneRollStrike() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(2);
		calculator.roll(1);
		assertEquals(275, calculator.score());
	}
	
	@Test(expected=TooManyRollException.class)
	public void shouldNotAddBonusRollInFinalRoundWhenRoundScoreLess10() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(2);
		calculator.roll(2);
		calculator.roll(1);
	}
	
	@Test(expected=TooManyRollException.class)
	public void shouldThrowTooManyRollExceptionWhenGameIsFinished() throws Exception {
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(10);
		calculator.roll(8);
		calculator.roll(2);
		calculator.roll(1);
		calculator.roll(2);
	}
	
	@Test(expected=TooManyPointsException.class)
	public void shouldThrowTooManyPointExceptionWhenRollSumOver10() throws Exception{
		calculator.roll(4);
		calculator.roll(7);
	}
	
	@Test(expected = IncorrectRollValueException.class)
	public void shouldThrowIncorrectRollValueExceptionWhenRollValueIsLess0() throws Exception {
		calculator.roll(-1);
	}
	
	@Test(expected = IncorrectRollValueException.class)
	public void shouldThrowIncorrectRollValueExceptionWhenRollValueIsOver10() throws Exception {
		calculator.roll(11);
	}
}
