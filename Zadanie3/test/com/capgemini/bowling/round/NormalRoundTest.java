package com.capgemini.bowling.round;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.bowling.round.NormalRound;
import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class NormalRoundTest {

	NormalRound normalRound;

	@Before
	public void setUp() throws Exception {
		normalRound = new NormalRound();
	}

	@Test
	public void shouldAddOnePointToList() throws Exception {
		normalRound.addPointToList(2);
		List<Integer> points = normalRound.getPoints();

		assertEquals(1, points.size());
	}

	@Test
	public void shouldAddTwoPointsToList() throws Exception {
		normalRound.addPointToList(2);
		normalRound.addPointToList(4);
		List<Integer> points = normalRound.getPoints();

		assertEquals(2, points.size());
	}

	@Test(expected = TooManyPointsException.class)
	public void shouldNotAddPointToListWhenSecondScoreIsToBig() throws Exception {
		normalRound.addPointToList(2);
		normalRound.addPointToList(9);
		List<Integer> points = normalRound.getPoints();
		assertEquals(1, points.size());
	}

	@Test(expected = TooManyRollException.class)
	public void shouldExceptWhenAddThreePointsToList() throws Exception {
		normalRound.addPointToList(2);
		normalRound.addPointToList(2);
		normalRound.addPointToList(2);
	}

	@Test(expected=IncorrectRollValueException.class)
	public void shouldThrowIncorrectRollValueExceptionWhenPointIsBiggerThen10() throws Exception {
		normalRound.addPointToList(11);
	}

	@Test
	public void sholudNotEndRoundWhenSumOfPointsIsLessThen10() throws Exception {
		normalRound.addPointToList(3);
		normalRound.isFinished();
		assertFalse(normalRound.getRoundFinished());
	}

	@Test
	public void sholudEndRoundWhenSumOfPointsIs10() throws Exception {
		normalRound.addPointToList(10);
		normalRound.isFinished();
		assertTrue(normalRound.getRoundFinished());
	}

	@Test
	public void sholudEndRoundWhenCountOfRollIsEqualTwo() throws Exception {
		normalRound.addPointToList(4);
		normalRound.addPointToList(4);
		normalRound.isFinished();
		assertTrue(normalRound.getRoundFinished());
	}

	@Test
	public void sholudNotAddPointToListWhenRoundIsFinishedOneRoll() throws Exception {
		normalRound.addPointToList(10);
		normalRound.isFinished();
		normalRound.addPointToList(7);
		List<Integer> points = normalRound.getPoints();
		assertEquals(1, points.size());
	}
}
