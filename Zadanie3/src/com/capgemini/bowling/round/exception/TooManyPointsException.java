package com.capgemini.bowling.round.exception;

public class TooManyPointsException extends Exception {

	public TooManyPointsException() {
		super("Wprowadzono za duza liczbe punktow w drugim rzucie");
	}
}
