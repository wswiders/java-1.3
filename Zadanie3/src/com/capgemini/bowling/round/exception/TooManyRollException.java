package com.capgemini.bowling.round.exception;

public class TooManyRollException extends Exception {

	public TooManyRollException() {
		super("Wykonano za duzo rzutow.");
	}

}
