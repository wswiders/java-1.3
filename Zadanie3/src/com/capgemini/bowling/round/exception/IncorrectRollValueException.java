package com.capgemini.bowling.round.exception;

public class IncorrectRollValueException extends Exception {
	public IncorrectRollValueException() {
		super("Wprowadzono nieprawidlowa liczbe punktow.");
	}
}
