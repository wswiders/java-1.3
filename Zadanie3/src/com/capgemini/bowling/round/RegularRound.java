package com.capgemini.bowling.round;

import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class RegularRound {

	private int id;
	private NormalRound normalRound;
	private BonusRound bonusRound;

	public RegularRound(int id) {
		this.id = id;
		normalRound = new NormalRound();
	}

	private void addRollToBonusRound(int roll)
			throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		if (bonusRound != null) {
			if (!bonusRound.getRoundFinished()) {
				bonusRound.addPointToList(roll);
				bonusRound.isFinished();
			}
		}
	}

	private void addRollToNormalRound(int roll)
			throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		if (!normalRound.getRoundFinished()) {
			normalRound.addPointToList(roll);
			normalRound.isFinished();
		}
	}

	public NormalRound getNormalRound() {
		return normalRound;
	}

	public int getRoundScore() {
		int sum = normalRound.getSumOfPoints();
		if (bonusRound != null) {
			sum += bonusRound.getSumOfPoints();
		}
		return sum;
	}

	private void initBonusRound() {
		if (normalRound.getRoundFinished() && bonusRound == null) {
			if (normalRound.getSumOfPoints() == 10 && normalRound.getPoints().size() == 1) {
				bonusRound = new BonusRound(2);
			} else if (normalRound.getSumOfPoints() == 10) {
				bonusRound = new BonusRound(1);
			}
		}
	}

	public void updateRound(int roll) throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		initBonusRound();
		addRollToNormalRound(roll);
		addRollToBonusRound(roll);
	}

}
