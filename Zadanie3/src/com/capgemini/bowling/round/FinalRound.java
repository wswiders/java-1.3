package com.capgemini.bowling.round;

import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class FinalRound extends NormalRound {

	private int numberOfThrows;

	public FinalRound() {
		super();
		numberOfThrows = 3;
	}

	@Override
	public void addPointToList(int point)
			throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		if (points.size() == 3) {
			throw new TooManyRollException();
		}
		if (!roundFinished) {
			if (point >= 0 && point <= 10) {
				points.add(point);
				isSecondRollCorrect();
				this.numberOfThrows--;
			} else {
				throw new IncorrectRollValueException();
			}
		}
	}

	private void isSecondRollCorrect() throws TooManyPointsException {
		if (points.size() == 2) {
			if ((points.get(0) + points.get(1)) > 10 && points.get(0) != 10) {
				points.remove(1);
				throw new TooManyPointsException();
			}
		}
	}

	@Override
	public void isFinished() {
		if ((numberOfThrows == 1 && super.getSumOfPoints() < 10) || numberOfThrows == 0) {
			this.roundFinished = true;
		}
	}
}
