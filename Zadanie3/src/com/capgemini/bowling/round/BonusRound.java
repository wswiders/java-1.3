package com.capgemini.bowling.round;

import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class BonusRound extends NormalRound {

	private int numberOfBonusRoll;

	public BonusRound(int numOfBonusRoll) {
		super();
		this.numberOfBonusRoll = numOfBonusRoll;
	}

	@Override
	public void addPointToList(int point) throws TooManyRollException, IncorrectRollValueException {
		if (points.size() == 2) {
			throw new TooManyRollException();
		}
		if (!roundFinished) {
			if (point >= 0 && point <= 10) {
				points.add(point);
				this.numberOfBonusRoll--;
			} else {
				throw new IncorrectRollValueException();
			}
		}
	}

	@Override
	public void isFinished() {
		if (numberOfBonusRoll == 0) {
			this.roundFinished = true;
		}
	}
}
