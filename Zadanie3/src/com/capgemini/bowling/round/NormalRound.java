package com.capgemini.bowling.round;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class NormalRound {

	protected boolean roundFinished;
	protected List<Integer> points;

	public NormalRound() {
		roundFinished = false;
		points = new ArrayList<>();
	}

	protected void addPointToList(int point)
			throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		if (points.size() == 2) {
			throw new TooManyRollException();
		}
		if (!roundFinished) {
			if (point >= 0 && point <= 10) {
				points.add(point);
				if (getSumOfPoints() > 10) {
					throw new TooManyPointsException();
				}
			} else {
				throw new IncorrectRollValueException();
			}
			
		}
	}

	protected List<Integer> getPoints() {
		return points;
	}

	public boolean getRoundFinished() {
		return roundFinished;
	}

	public int getSumOfPoints() {
		int sum = 0;
		for (Integer point : this.points) {
			sum += point;
		}
		return sum;
	}

	protected void isFinished() {
		if (getSumOfPoints() == 10 || this.points.size() == 2) {
			this.roundFinished = true;
		}
	}

}
