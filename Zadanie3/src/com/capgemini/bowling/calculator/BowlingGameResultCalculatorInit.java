package com.capgemini.bowling.calculator;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.bowling.round.FinalRound;
import com.capgemini.bowling.round.RegularRound;
import com.capgemini.bowling.round.exception.IncorrectRollValueException;
import com.capgemini.bowling.round.exception.TooManyPointsException;
import com.capgemini.bowling.round.exception.TooManyRollException;

public class BowlingGameResultCalculatorInit implements BowlingGameResultCalculator {

	private List<RegularRound> regularRounds;
	private int currentRound;
	private FinalRound finalRound;

	public BowlingGameResultCalculatorInit() {
		regularRounds = new ArrayList<>();
		currentRound = 0;
		regularRounds.add(new RegularRound(currentRound));
	}

	private void addPointsToRounds(int numberOfPins)
			throws TooManyPointsException, TooManyRollException, IncorrectRollValueException {
		for (RegularRound round : regularRounds) {
			round.updateRound(numberOfPins);
		}

		if (finalRound != null) {
			if (!finalRound.getRoundFinished()) {
				finalRound.addPointToList(numberOfPins);
				finalRound.isFinished();
			}
		}
	}

	@Override
	public boolean isFinished() {
		if (finalRound != null && finalRound.getRoundFinished()) {
			return true;
		}
		return false;
	}

	@Override
	public void roll(int numberOfPins) throws Exception {
		if (this.isFinished()) {
			throw new TooManyRollException();
		}
		roundInit();
		addPointsToRounds(numberOfPins);
	}

	private void roundInit() {
		if (regularRounds.get(currentRound).getNormalRound().getRoundFinished()) {
			if (currentRound < 8) {
				regularRounds.add(new RegularRound(++currentRound));
			} else if (finalRound == null) {
				finalRound = new FinalRound();
			}
		}
	}

	@Override
	public int score() {
		int sum = 0;
		for (RegularRound round : regularRounds) {
			sum += round.getRoundScore();
		}
		if (finalRound != null) {
			sum += finalRound.getSumOfPoints();
		}
		return sum;
	}
}
